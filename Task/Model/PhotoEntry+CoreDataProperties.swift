//
//  PhotoEntry+CoreDataProperties.swift
//  Task
//
//  Created by KOVIGROUP on 22/02/2022.
//

import Foundation
import CoreData


extension PhotoEntry {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PhotoEntry> {
        return NSFetchRequest<PhotoEntry>(entityName: "PhotoEntity")
    }

    @NSManaged public var entryDescription: String?
    @NSManaged public var entryDate: Date?
    @NSManaged public var photoImage: Data?

}

extension PhotoEntry : Identifiable {

}
