//
//  DatabaseHelper.swift
//  Task
//
//  Created by KOVIGROUP on 22/02/2022.
//

import UIKit

class DatabaseHelper {
    
    static let shared = DatabaseHelper()
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    func savePhotoEntry(image: UIImage, description: String, date: Date) {
        
        guard let imageData = image.pngData() else { return }
        
        let photoEntry = PhotoEntry(context: context)
        photoEntry.photoImage = imageData
        photoEntry.entryDescription = description
        photoEntry.entryDate = date
        
        do {
            try context.save()
        } catch {
            print(error)
        }
    }
    
    func fetchPhotoEntry() -> Array<PhotoEntry> {
        
        var photoJournal = Array<PhotoEntry>()
        
        do {
            photoJournal = try context.fetch(PhotoEntry.fetchRequest())
            print("Data fetched")
            
        } catch {
            print(error)
        }
        
        return photoJournal
    }
}

