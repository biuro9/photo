//
//  PhotoEntryCell.swift
//  Task
//
//  Created by KOVIGROUP on 22/02/2022.
//

import UIKit

class PhotoEntryCell: UITableViewCell {

    let photoImage: UIImageView = {
        let image = UIImageView()
        image.backgroundColor = .systemPink
        return image
    }()
    
    let photoDescription: UILabel = {
        let description = UILabel()
        description.textColor = .black
        description.numberOfLines = 0
        return description
    }()
    
    let entryDate: UILabel = {
        let date = UILabel()
        date.textColor = .lightGray
        return date
        
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = .white
        
        contentView.addSubview(photoImage)
        photoImage.translatesAutoresizingMaskIntoConstraints = false
        photoImage.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10.0).isActive = true
        photoImage.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10.0).isActive = true
        photoImage.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10.0).isActive = true
        photoImage.addConstraint(NSLayoutConstraint(item: photoImage, attribute: .width, relatedBy: .equal, toItem: photoImage, attribute: .height, multiplier: 1.0, constant: 0))
        
        contentView.addSubview(photoDescription)
        photoDescription.translatesAutoresizingMaskIntoConstraints = false
        photoDescription.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 30.0).isActive = true
        photoDescription.leadingAnchor.constraint(equalTo: photoImage.trailingAnchor, constant: 20.0).isActive = true
        photoDescription.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0).isActive = true
        
        contentView.addSubview(entryDate)
        entryDate.translatesAutoresizingMaskIntoConstraints = false
        entryDate.leadingAnchor.constraint(equalTo: photoImage.trailingAnchor, constant: 20.0).isActive = true
        entryDate.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0).isActive = true
        entryDate.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -30.0).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

