//
//  AddPhotoViewController.swift
//  Task
//
//  Created by KOVIGROUP on 22/02/2022.
//

import UIKit

class AddPhotoViewController: UIViewController {
    
    private let photoImage: UIImageView = {
        let image = UIImageView()
        image.backgroundColor = .lightGray
        
        return image
    }()
    
    private let descriptionFieldLabel: UILabel = {
        let label = UILabel()
        label.text = "Add description"
        label.textColor = .lightGray
        return label
    }()
    
    private let descriptionField: UITextField = {
        let textField = UITextField()
        textField.textColor = .black
        textField.placeholder = "Name your photo"
        textField.backgroundColor = .white
        textField.borderStyle = .roundedRect
        return textField
    }()
    
    private let doneButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor(red: CGFloat(66.0/255.0), green: CGFloat(133.0/255.0), blue: CGFloat(244.0/255.0), alpha: 1)
        button.setTitle("Done", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.setTitleColor(.lightGray, for: .highlighted)
        button.layer.cornerRadius = 5.0
        return button
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        addHiddingKeyboard()
    
        self.navigationItem.title = "Photo entry"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .camera, target: self, action: #selector(takePhoto))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(dismissView))
        
        view.addSubview(photoImage)
        photoImage.translatesAutoresizingMaskIntoConstraints = false
        photoImage.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 30.0).isActive = true
        photoImage.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 30.0).isActive = true
        photoImage.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -30.0).isActive = true
        photoImage.addConstraint(NSLayoutConstraint(item: photoImage, attribute: .height, relatedBy: .equal, toItem: photoImage, attribute: .width, multiplier: 1.0, constant: 0.0))
        
        view.addSubview(descriptionFieldLabel)
        descriptionFieldLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionFieldLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 30.0).isActive = true
        descriptionFieldLabel.topAnchor.constraint(equalTo: photoImage.bottomAnchor, constant: 30.0).isActive = true
        
        view.addSubview(descriptionField)
        descriptionField.translatesAutoresizingMaskIntoConstraints = false
        descriptionField.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 30.0).isActive = true
        descriptionField.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -30.0).isActive = true
        descriptionField.topAnchor.constraint(equalTo: descriptionFieldLabel.bottomAnchor, constant: 15.0).isActive = true
        
        view.addSubview(doneButton)
        doneButton.translatesAutoresizingMaskIntoConstraints = false
        doneButton.heightAnchor.constraint(equalToConstant: 45.0).isActive = true
        doneButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 30).isActive = true
        doneButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -30).isActive = true
        doneButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -30.0).isActive = true
        doneButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        doneButton.addTarget(self, action: #selector(addPhotoJournal), for: .touchUpInside)
        
    }

    @objc func addPhotoJournal() {
        guard let image = photoImage.image, let description = descriptionField.text else { return }
        DatabaseHelper.shared.savePhotoEntry(image: image, description: description, date: Date())
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func takePhoto() {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        picker.sourceType = .camera
        picker.modalPresentationStyle = .fullScreen
        present(picker, animated: true, completion: nil)
    }
    
    @objc func dismissView() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension AddPhotoViewController: UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let pickedImage = info[.editedImage] as? UIImage else { return }
        
        self.photoImage.image = pickedImage
        picker.dismiss(animated: true, completion: nil)
    }
}

extension AddPhotoViewController {
    func addHiddingKeyboard() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
}

