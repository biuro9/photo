//
//  PhotoEntryDetailViewControllerViewController.swift
//  Task
//
//  Created by KOVIGROUP on 22/02/2022.
//

import UIKit

class PhotoEntryDetailViewControllerViewController: UIViewController {
    
    let photoImage: UIImageView = {
        let image = UIImageView()
        image.backgroundColor = .lightGray // Change it later
        
        return image
    }()
    
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = "Description:"
        label.textColor = .lightGray
        return label
    }()
    
    let entryDescription: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.numberOfLines = 0
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        self.navigationItem.title = "Photo entry"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(dismissView))
        
        view.addSubview(photoImage)
        photoImage.translatesAutoresizingMaskIntoConstraints = false
        photoImage.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 30.0).isActive = true
        photoImage.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 30.0).isActive = true
        photoImage.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -30.0).isActive = true
        photoImage.addConstraint(NSLayoutConstraint(item: photoImage, attribute: .height, relatedBy: .equal, toItem: photoImage, attribute: .width, multiplier: 1.0, constant: 0.0))
        
        view.addSubview(descriptionLabel)
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 30.0).isActive = true
        descriptionLabel.topAnchor.constraint(equalTo: photoImage.bottomAnchor, constant: 30.0).isActive = true
        
        view.addSubview(entryDescription)
        entryDescription.translatesAutoresizingMaskIntoConstraints = false
        entryDescription.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 30.0).isActive = true
        entryDescription.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -30.0).isActive = true
        entryDescription.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 10.0).isActive = true
        
    }
    
    @objc func dismissView() {
        self.dismiss(animated: true, completion: nil)
    }
}

