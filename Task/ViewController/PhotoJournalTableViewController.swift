//
//  PhotoJournalTableViewController.swift
//  Task
//
//  Created by KOVIGROUP on 22/02/2022.
//

import UIKit

class PhotoJournalTableViewController: UITableViewController {

    var photoEntries = [PhotoEntry]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        photoEntries = DatabaseHelper.shared.fetchPhotoEntry()
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = 150.0
        tableView.separatorStyle = .none
        
        view.backgroundColor = .white
        self.navigationItem.title = "My photo"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addNewPhotoEntry))

    }
    
    override func viewWillAppear(_ animated: Bool) {
        photoEntries = DatabaseHelper.shared.fetchPhotoEntry()
        self.tableView.reloadData()
    }

    @objc func addNewPhotoEntry() {
        let vc = UINavigationController(rootViewController: AddPhotoViewController())
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photoEntries.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = PhotoEntryCell()
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        
        cell.photoImage.image = UIImage(data: photoEntries[indexPath.row].photoImage!)
        cell.photoDescription.text = photoEntries[indexPath.row].entryDescription
        cell.entryDate.text = dateFormatter.string(from: photoEntries[indexPath.row].entryDate!)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = PhotoEntryDetailViewControllerViewController()
        vc.photoImage.image = UIImage(data: photoEntries[indexPath.row].photoImage!)
        vc.entryDescription.text = photoEntries[indexPath.row].entryDescription
        let nc = UINavigationController(rootViewController: vc)
        nc.modalPresentationStyle = .popover
        present(nc, animated: true, completion: nil)
        
    }
}
